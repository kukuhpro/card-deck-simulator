// Package util implements a simple library for help code to make it easy to maintain and extends
package util

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

type QueryBuilderContract interface {
	Where() (string, []interface{})
	Order() string
	Limit() string
}

type queryBuilder struct {
	filter  map[string]interface{}
	orderBy string
	limit   int
}

func (q *queryBuilder) Order() string {
	if q.orderBy == "random" {
		return " ORDER BY RANDOM()"
	}
	return ""
}

func (q *queryBuilder) Limit() string {
	return " LIMIT " + fmt.Sprint(q.limit)
}

func (q *queryBuilder) Where() (string, []interface{}) {
	var wheres []string
	args := []interface{}{}
	r, _ := regexp.Compile("[a-z]*[$](in|eq)")
	for k, v := range q.filter {
		if splitKey := strings.Split(k, "$"); r.MatchString(k) && len(splitKey) > 1 {
			column := splitKey[0]
			operator := splitKey[1]
			switch operator {
			case "in":
				reflectKind := reflect.TypeOf(v).Kind()
				if reflectKind == reflect.Array || reflectKind == reflect.Slice {
					vArr := reflect.ValueOf(v)
					txtValIn := ""
					for i := 0; i < vArr.Len(); i++ {
						if i == 0 {
							txtValIn += "?"
						} else {
							txtValIn += ", ?"
						}
						args = append(args, vArr.Index(i).Interface())
					}
					wheres = append(wheres, fmt.Sprintf("%s IN (%s)", column, txtValIn))
				}
			case "eq":
				wheres = append(wheres, fmt.Sprintf("%s = ?", column))
				args = append(args, v)
			}
		}
	}
	if len(wheres) > 0 {
		return " WHERE " + strings.Join(wheres, " AND "), args
	}
	return "", []interface{}{}
}

func NewQueryBuilder(limit int, orderBy string, filter map[string]interface{}) QueryBuilderContract {
	var qb queryBuilder
	qb.filter = filter
	qb.limit = limit
	qb.orderBy = orderBy
	return &qb
}
