package util_test

import (
	"card-deck-simulation/util"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"syreclabs.com/go/faker"
)

type QueryBuilderSuite struct {
	suite.Suite
}

func (q *QueryBuilderSuite) SetupTest() {}

func TestQueryBuilderSuite(t *testing.T) {
	suite.Run(t, new(QueryBuilderSuite))
}

func (q *QueryBuilderSuite) TestWhereWithOnMultipleCase() {
	fakerWhereInValue := faker.Address().String()
	fakerWhereInValue2 := faker.Address().String()
	listTestData := []map[string]interface{}{
		{
			"data": map[string]interface{}{
				"name$in": []string{fakerWhereInValue},
			},
			"expectedQueryWhere": " WHERE name IN (?)",
			"expectedArgs":       []interface{}{fakerWhereInValue},
		},
		{
			"data": map[string]interface{}{
				"name$eq": fakerWhereInValue,
			},
			"expectedQueryWhere": " WHERE name = ?",
			"expectedArgs":       []interface{}{fakerWhereInValue},
		},
		{
			"data": map[string]interface{}{
				"code$in": []string{fakerWhereInValue, fakerWhereInValue2},
				"name$eq": fakerWhereInValue,
			},
			"expectedQueryWhere": " WHERE code IN (?, ?) AND name = ?",
			"expectedArgs":       []interface{}{fakerWhereInValue, fakerWhereInValue2, fakerWhereInValue},
		},
		{
			"data": map[string]interface{}{
				"name$az": []string{fakerWhereInValue},
			},
			"expectedQueryWhere": "",
			"expectedArgs":       []interface{}{},
		},
	}

	for _, testData := range listTestData {
		qb := util.NewQueryBuilder(0, "", testData["data"].(map[string]interface{}))
		resultQuery, resultArgs := qb.Where()
		require.Equal(q.T(), testData["expectedQueryWhere"], resultQuery)
		require.EqualValues(q.T(), testData["expectedArgs"], resultArgs)
	}
}

func (q *QueryBuilderSuite) TestOrderWithLimitOnMultipleCase() {
	fakerLimitValue := faker.Number().NumberInt(1)
	fakerOrderValue := "random"

	listTestData := []map[string]interface{}{
		{
			"dataFilter":    map[string]interface{}{},
			"dataLimit":     fakerLimitValue,
			"dataOrder":     fakerOrderValue,
			"expectedQuery": fmt.Sprintf(" ORDER BY RANDOM() LIMIT %d", fakerLimitValue),
		},
		{
			"dataFilter":    map[string]interface{}{},
			"dataLimit":     fakerLimitValue,
			"dataOrder":     "name ASC",
			"expectedQuery": fmt.Sprintf(" LIMIT %d", fakerLimitValue),
		},
	}

	for _, testData := range listTestData {
		qb := util.NewQueryBuilder(testData["dataLimit"].(int), testData["dataOrder"].(string), testData["dataFilter"].(map[string]interface{}))
		resultQuery := qb.Order() + qb.Limit()
		require.Equal(q.T(), testData["expectedQuery"], resultQuery)
	}
}
