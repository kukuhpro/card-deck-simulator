package util_test

import (
	"card-deck-simulation/util"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"syreclabs.com/go/faker"
)

type HttpErrorSuite struct {
	suite.Suite
}

func (h *HttpErrorSuite) SetupTest() {

}

func TestHttpErrorSuite(t *testing.T) {
	suite.Run(t, new(HttpErrorSuite))
}

func (h *HttpErrorSuite) TestHttpErrorShouldSuccess() {
	dataErr := errors.New(faker.Address().String())
	dataStatus := faker.Number().NumberInt(3)
	dataDetail := faker.Address().String()
	listTestData := []map[string]interface{}{
		{
			"dataErr":                      dataErr,
			"dataStatus":                   dataStatus,
			"dataDetail":                   dataDetail,
			"expectedErrorValue":           fmt.Sprintf("%s : %s", dataDetail, dataErr.Error()),
			"expectedResponseBody":         []byte(fmt.Sprintf(`{"cause":{},"detail":"%s","code_status":%d}`, dataDetail, dataStatus)),
			"expectedResponseHeaderStatus": dataStatus,
			"expectedResponseHeaderData": map[string]string{
				"Content-Type": "application/json; charset=utf-8",
			},
		},
		{
			"dataErr":                      nil,
			"dataStatus":                   dataStatus,
			"dataDetail":                   dataDetail,
			"expectedErrorValue":           dataDetail,
			"expectedResponseBody":         []byte(fmt.Sprintf(`{"cause":null,"detail":"%s","code_status":%d}`, dataDetail, dataStatus)),
			"expectedResponseHeaderStatus": dataStatus,
			"expectedResponseHeaderData": map[string]string{
				"Content-Type": "application/json; charset=utf-8",
			},
		},
	}

	for _, testData := range listTestData {
		var testDataErr error
		if testData["dataErr"] != nil {
			testDataErr = testData["dataErr"].(error)
		}
		httpError := &util.Error{
			Cause:  testDataErr,
			Detail: testData["dataDetail"].(string),
			Status: testData["dataStatus"].(int),
		}
		resultError := httpError.Error()
		resultBodyBytes, _ := httpError.ResponseBody()
		resultStatus, resultHeaders := httpError.ResponseHeaders()
		bytesExpectedBody := testData["expectedResponseBody"].([]byte)
		require.Equal(h.T(), testData["expectedErrorValue"], resultError)
		require.Equal(h.T(), string(bytesExpectedBody), string(resultBodyBytes))
		require.Equal(h.T(), testData["expectedResponseHeaderStatus"], resultStatus)
		require.Equal(h.T(), testData["expectedResponseHeaderData"], resultHeaders)
	}
}

func (h *HttpErrorSuite) TestConstructorShouldSuccess() {
	httpError := util.NewError(nil, 0, "")
	expectedType := util.Error{}
	require.IsType(h.T(), &expectedType, httpError)
}
