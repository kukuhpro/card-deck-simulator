package util

import (
	"encoding/json"
	"fmt"
)

type Error struct {
	error
	Cause  error  `json:"cause"`
	Detail string `json:"detail"`
	Status int    `json:"code_status"`
}

func (e *Error) Error() string {
	if e.Cause == nil {
		return e.Detail
	}
	return e.Detail + " : " + e.Cause.Error()
}

func (e *Error) ResponseBody() ([]byte, error) {
	body, err := json.Marshal(e)
	if err != nil {
		return nil, fmt.Errorf("Error while parsing response body: %v", err)
	}
	return body, nil
}

func (e *Error) ResponseHeaders() (int, map[string]string) {
	return e.Status, map[string]string{
		"Content-Type": "application/json; charset=utf-8",
	}
}

func NewError(err error, status int, detail string) error {
	return &Error{
		Cause:  err,
		Detail: detail,
		Status: status,
	}
}
