// This package is where Http running to Serve Endpoint
// and calling action
package http

import (
	"card-deck-simulation/action"
	"card-deck-simulation/entity"
	"card-deck-simulation/util"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
)

func Serve() {
	log.Println("START HTTP SERVER...")
	e := echo.New()
	e.POST("/deck", func(c echo.Context) error {
		var req entity.CreateDeckRequest
		if err := c.Bind(&req); err != nil {
			return err
		}
		createDeckAction := action.NewCreateDeck()
		res, err := createDeckAction.Handle(c.Request().Context(), req)
		if err != nil {
			resErr := err.(*util.Error)
			c.JSON(resErr.Status, resErr)
			return nil
		}
		c.JSON(http.StatusOK, &res)
		return nil
	})
	e.GET("/deck/:id", func(c echo.Context) error {
		deckId := c.Param("id")
		var req entity.OpenDeckRequest
		var resErr *util.Error
		req.DeckID = deckId

		openDeckAction := action.NewOpenDeck()
		res, err := openDeckAction.Handle(c.Request().Context(), req)
		if err != nil {
			resErr = err.(*util.Error)
			c.JSON(resErr.Status, resErr)
			return nil
		}
		c.JSON(http.StatusOK, &res)
		return nil
	})
	e.PUT("/deck/:id", func(c echo.Context) error {
		var req entity.DrawCardRequest
		deckId := c.Param("id")

		if err := c.Bind(&req); err != nil {
			return err
		}
		req.DeckID = deckId
		drawCardAction := action.NewDrawCard()

		res, err := drawCardAction.Handle(c.Request().Context(), req)
		if err != nil {
			resErr := err.(*util.Error)
			c.JSON(resErr.Status, resErr)
			return nil
		}
		c.JSON(http.StatusOK, &res)
		return nil
	})
	errServer := e.Start(":8080")
	if errServer != nil {
		panic(errServer)
	}
	log.Println("Http Server running on :8080")
}
