// entity package is struct of object that work as Glue object between action and repo
package entity

type Card struct {
	Value string `json:"value"`
	Suit  string `json:"suit"`
	Code  string `json:"code"`
}

type CreateDeckRequest struct {
	Shuffled bool   `json:"shuffled"`
	Cards    string `json:"cards"`
}

type DrawCardRequest struct {
	DeckID string `json:"deck_id" validate:"required,uuid"`
	Count  int    `json:"count" validate:"required,numeric,gt=0,lte=52"`
}

type Deck struct {
	DeckID    string `json:"deck_id,omitempty"`
	Shuffled  bool   `json:"shuffled,omitempty"`
	Remaining int    `json:"remaining,omitempty"`
	Cards     []Card `json:"cards,omitempty"`
}

type OpenDeckRequest struct {
	DeckID string `json:"deck_id" validate:"required,uuid"`
}
