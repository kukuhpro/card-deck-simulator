FROM golang:1.16-alpine

RUN mkdir /app
RUN apk add gcc libtool musl-dev 

WORKDIR /app
COPY . /app/

COPY ./run.sh /app/run.sh

CMD ["/app/run.sh"]

RUN chmod 755 /app/run.sh
RUN chmod +x /app/run.sh

EXPOSE 8080
