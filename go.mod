module card-deck-simulation

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-playground/validator/v10 v10.9.0
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/jmoiron/sqlx v1.3.4
	github.com/labstack/echo/v4 v4.5.0
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
	syreclabs.com/go/faker v1.2.3
)
