package main

import (
	"card-deck-simulation/repo/postgresql"
	"card-deck-simulation/transport/http"
)

func main() {
	// Calling to setup database connection and running migrations for database
	postgresql.Setup()

	// calling to running http server on port :8080
	http.Serve()
}
