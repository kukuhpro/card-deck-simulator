package action_test

import (
	"card-deck-simulation/action"
	"card-deck-simulation/entity"
	"card-deck-simulation/repo/mocks"
	"card-deck-simulation/util"
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type OpenDeckSuiteTest struct {
	suite.Suite
	deckRepo       *mocks.DeckContract
	openDeckAction *action.OpenDeck
}

func (o *OpenDeckSuiteTest) SetupTest() {
	o.deckRepo = new(mocks.DeckContract)
	o.openDeckAction = action.NewOpenDeck()
	o.openDeckAction.DeckRepo = o.deckRepo
}

func TestOpenDeckSuite(t *testing.T) {
	suite.Run(t, new(OpenDeckSuiteTest))
}

func (o *OpenDeckSuiteTest) TestHandleShouldSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	u1 := uuid.Must(uuid.NewV1(), nil)

	var req entity.OpenDeckRequest
	req.DeckID = u1.String()

	mockRepo := o.deckRepo

	var responseRepo entity.Deck
	responseRepo.DeckID = req.DeckID

	mockRepo.On("Get", ctx, req).Return(responseRepo, nil)

	res, err := o.openDeckAction.Handle(ctx, req)
	require.Equal(o.T(), nil, err)
	require.Equal(o.T(), req.DeckID, res.DeckID)
}

func (o *OpenDeckSuiteTest) TestHandleShouldError() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	u1 := uuid.Must(uuid.NewV1(), nil)

	var req entity.OpenDeckRequest
	req.DeckID = u1.String()

	mockRepo := o.deckRepo

	var responseMockRepo entity.Deck

	mockRepo.On("Get", ctx, req).Return(responseMockRepo, util.NewError(errors.New(""), http.StatusNotFound, ""))

	_, err := o.openDeckAction.Handle(ctx, req)
	require.IsType(o.T(), &util.Error{}, err)
	resErr := err.(*util.Error)
	require.Equal(o.T(), http.StatusNotFound, resErr.Status)
}
