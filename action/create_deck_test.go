package action_test

import (
	"card-deck-simulation/action"
	"card-deck-simulation/entity"
	"card-deck-simulation/repo/mocks"
	"card-deck-simulation/util"
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type CreateDeckSuiteTest struct {
	suite.Suite
	deckRepoMock     *mocks.DeckContract
	createDeckAction *action.CreateDeck
}

func (c *CreateDeckSuiteTest) SetupTest() {
	c.deckRepoMock = new(mocks.DeckContract)
	c.createDeckAction = action.NewCreateDeck()
	c.createDeckAction.DeckRepo = c.deckRepoMock
}

func TestCreateDeckSuite(t *testing.T) {
	suite.Run(t, new(CreateDeckSuiteTest))
}

func (c *CreateDeckSuiteTest) TestHandleShouldSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var req entity.CreateDeckRequest
	req.Cards = "a,b"
	req.Shuffled = true
	deckRepoMock := c.deckRepoMock

	responseRepo := []entity.Deck{}

	u1 := uuid.Must(uuid.NewV1(), nil)

	deckID := u1.String()
	responseRepo = append(responseRepo, entity.Deck{
		DeckID: deckID,
	})

	deckRepoMock.On("CreateDeck", ctx, req).Return(responseRepo, nil)

	res, err := c.createDeckAction.Handle(ctx, req)
	require.Equal(c.T(), nil, err)
	require.Equal(c.T(), deckID, res.DeckID)
}

func (c *CreateDeckSuiteTest) TestHandleShouldError() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var req entity.CreateDeckRequest

	responseMock := []entity.Deck{}

	c.deckRepoMock.On("CreateDeck", ctx, req).Return(responseMock, util.NewError(errors.New(""), http.StatusBadRequest, ""))

	_, err := c.createDeckAction.Handle(ctx, req)

	require.IsType(c.T(), &util.Error{}, err)
	resErr := err.(*util.Error)
	require.Equal(c.T(), http.StatusBadRequest, resErr.Status)
}
