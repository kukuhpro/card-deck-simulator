package action

import (
	"card-deck-simulation/entity"
	"card-deck-simulation/repo"
	"card-deck-simulation/util"
	"context"
	"net/http"
)

type OpenDeck struct {
	DeckRepo repo.DeckContract
}

func (o *OpenDeck) Handle(ctx context.Context, req entity.OpenDeckRequest) (response entity.Deck, err error) {
	err = util.Validate.Struct(&req)
	if err != nil {
		return response, util.NewError(err, http.StatusBadRequest, "Request is Invalid")
	}
	response, err = o.DeckRepo.Get(ctx, req)
	if err != nil {
		return response, util.NewError(err, http.StatusNotFound, "Data is not found")
	}
	return
}

func NewOpenDeck() *OpenDeck {
	var o OpenDeck
	o.DeckRepo = repo.NewDeckRepo()
	return &o
}
