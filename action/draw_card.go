package action

import (
	"card-deck-simulation/entity"
	"card-deck-simulation/repo"
	"card-deck-simulation/util"
	"context"
	"net/http"
)

type DrawCard struct {
	DeckRepo repo.DeckContract
}

func (d *DrawCard) Handle(ctx context.Context, req entity.DrawCardRequest) (response entity.Deck, err error) {
	err = util.Validate.Struct(&req)
	if err != nil {
		return response, util.NewError(err, http.StatusBadRequest, "Request is Invalid")
	}
	response, err = d.DeckRepo.CardSubtract(ctx, req)
	if err != nil {
		return response, util.NewError(err, http.StatusBadRequest, "Invalid Request")
	}
	return
}

func NewDrawCard() *DrawCard {
	var d DrawCard
	d.DeckRepo = repo.NewDeckRepo()
	return &d
}
