// package focus on business logic and validation request
package action

import (
	"card-deck-simulation/entity"
	"card-deck-simulation/repo"
	"card-deck-simulation/util"
	"context"
	"net/http"
)

type CreateDeck struct {
	DeckRepo repo.DeckContract
}

func (c *CreateDeck) Handle(ctx context.Context, req entity.CreateDeckRequest) (response entity.Deck, err error) {
	err = util.Validate.Struct(&req)
	if err != nil {
		return response, util.NewError(err, http.StatusBadRequest, "Request is Invalid")
	}
	data, err := c.DeckRepo.CreateDeck(ctx, req)
	if len(data) <= 0 {
		return response, util.NewError(err, http.StatusBadRequest, "Failed to create deck")
	}
	return data[0], err
}

func NewCreateDeck() *CreateDeck {
	var c CreateDeck
	c.DeckRepo = repo.NewDeckRepo()
	return &c
}
