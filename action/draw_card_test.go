package action_test

import (
	"card-deck-simulation/action"
	"card-deck-simulation/entity"
	"card-deck-simulation/repo/mocks"
	"card-deck-simulation/util"
	"context"
	"errors"
	"net/http"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"syreclabs.com/go/faker"
)

type DrawCardSuiteTest struct {
	suite.Suite
	deckRepoMock   *mocks.DeckContract
	drawCardAction *action.DrawCard
}

func (d *DrawCardSuiteTest) SetupTest() {
	d.deckRepoMock = new(mocks.DeckContract)
	d.drawCardAction = action.NewDrawCard()
	d.drawCardAction.DeckRepo = d.deckRepoMock
}

func TestDrawCardSuite(t *testing.T) {
	suite.Run(t, new(DrawCardSuiteTest))
}

func (d *DrawCardSuiteTest) TestHandleShouldSuccess() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	u1 := uuid.Must(uuid.NewV1(), nil)

	var req entity.DrawCardRequest
	req.DeckID = u1.String()
	req.Count = faker.RandomInt(1, 52)

	mockRepo := d.deckRepoMock

	var card1 entity.Card
	card1.Code = faker.RandomString(2)
	card1.Suit = faker.RandomString(4)
	card1.Value = faker.RandomString(8)

	var repoResponse entity.Deck
	repoResponse.DeckID = req.DeckID
	repoResponse.Cards = append(repoResponse.Cards, card1)
	repoResponse.Remaining = len(repoResponse.Cards)
	repoResponse.Shuffled = false

	mockRepo.On("CardSubtract", ctx, req).Return(repoResponse, nil)

	res, err := d.drawCardAction.Handle(ctx, req)
	require.Equal(d.T(), nil, err)
	require.Equal(d.T(), req.DeckID, res.DeckID)
}

func (d *DrawCardSuiteTest) TestHandleShouldError() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	u1 := uuid.Must(uuid.NewV1(), nil)

	var req entity.DrawCardRequest
	req.DeckID = u1.String()
	req.Count = faker.RandomInt(1, 52)

	var responseMockRepo entity.Deck

	mockRepo := d.deckRepoMock
	mockRepo.On("CardSubtract", ctx, req).Return(responseMockRepo, util.NewError(errors.New(""), http.StatusBadRequest, ""))

	_, err := d.drawCardAction.Handle(ctx, req)
	require.IsType(d.T(), &util.Error{}, err)
	resErr := err.(*util.Error)

	require.Equal(d.T(), http.StatusBadRequest, resErr.Status)
}
