### Getting Started
This repository code requirement
* Go > 1.15.12
* PostgreSql 11
* Docker *(Running with Docker Compose)*
* Docker Compose CLI *(Running with Docker Compose)*

### Installation
If *running without Docker Compose*, install locally for Go and Postgresql, and set **environment variable** for configuration.
##### 1. Install GO and PostgreSQL
##### 2. Run 
`go mod tidy`
##### 3. Setup Environment Variable
To set **environment variable** run this script in your CLI
```
export postgresql_user={replace_with_your_postgresql_user} \
export postgresql_password={replace_with_your_postgresql_password} \
export postgresql_host={replace_with_your_postgresql_host} \
export postgresql_port=5432 \
export postgresql_database=postgres
```

### Running Test
To run test after installation
```
./run_test.sh
``` 

### Running without Docker
To run application without **Docker** 
```
./run.sh
```

### Running with Docker Compose
Make sure already installed Docker and Docker Compose CLI
```
docker-compose up
```

### Rest API Documentation
This application have 3 Action with 3 endpoint separately
#### Create a Deck
**Method:** POST

**URL:** `http://localhost:8080/deck`

**Header:**

```
{
    "Content-Type": "application/json"
}
```
**Body Request Parameter:**
| #        | description           | example | require  |
| ------------- |:-------------:| -----:| -----:|
| shuffled      | Send this parameter to make card for the deck is shuffled or not | `true` or `false` | yes |
| cards      | send this parameter to chouse specific cards only for the deck      | `KH,3C,AH,10D` | no |
```
{
    "shuffled": false,
    "cards": "KH,3C,AH,10D"
}
```

#### Open a Deck
**Method:** GET

**URL:** `http://localhost:8080/deck/<id>`

**Header:**

```
{
    "Content-Type": "application/json"
}
```
**URL Parameter:**
| variable        | description            | require  |
| ------------- |:-------------:| -----:| 
| id      | send `uuid` as value of deck id | yes |

#### Draw a Card
**Method:** PUT

**URL:** `http://localhost:8080/deck/<id>`

**Header:**

```
{
    "Content-Type": "application/json"
}
```

**URL Parameter:**
| variable        | description            | require  |
| ------------- |:-------------:| -----:| 
| id      | send `uuid` as value of deck id | yes |

**Body Request Parameter:**
| #        | description           | example | require  |
| ------------- |:-------------:| -----:| -----:|
| count      | send this value as number of card who like tp draw from the deck | `1`, `2` | yes |
```
{
    "count": 2
}
```















