// Repo package is related to all data manipulation and data logic
// it also contain database connection and database query
package repo

import (
	"card-deck-simulation/entity"
	"card-deck-simulation/repo/postgresql"
	"card-deck-simulation/util"
	"context"
	"errors"
	"strings"

	uuid "github.com/satori/go.uuid"
)

type DeckContract interface {
	CreateDeck(ctx context.Context, req entity.CreateDeckRequest) ([]entity.Deck, error)
	CardSubtract(ctx context.Context, req entity.DrawCardRequest) (entity.Deck, error)
	Get(ctx context.Context, req entity.OpenDeckRequest) (entity.Deck, error)
}

type deckRepo struct {
}

func (d *deckRepo) Get(ctx context.Context, req entity.OpenDeckRequest) (entity.Deck, error) {
	var responseDeck entity.Deck

	queryBuildergetDeck := util.NewQueryBuilder(0, "", map[string]interface{}{
		"id$eq": req.DeckID,
	})

	deck, err := postgresql.GetDeck(ctx, queryBuildergetDeck)

	if deck.ID == "" || err != nil {
		return responseDeck, err
	}

	responseDeck.DeckID = deck.ID
	responseDeck.Shuffled = deck.IsShuffle

	queryBuilderCardDeck := util.NewQueryBuilder(0, "", map[string]interface{}{
		"deck_id$eq":   req.DeckID,
		"has_drawn$eq": false,
	})

	listCardDeck, err := postgresql.ListCardDeck(ctx, queryBuilderCardDeck)
	if err != nil {
		return responseDeck, err
	}
	responseDeck.Remaining = len(listCardDeck)

	for _, d := range listCardDeck {
		var card entity.Card
		card.Code = d.CardCode
		card.Suit = d.Suit
		card.Value = d.Value
		responseDeck.Cards = append(responseDeck.Cards, card)
	}

	return responseDeck, nil
}

func (d *deckRepo) CardSubtract(ctx context.Context, req entity.DrawCardRequest) (entity.Deck, error) {
	var listResponseCardDeck entity.Deck
	listResponseCardDeck.DeckID = req.DeckID
	queryBuilderUpdate := util.NewQueryBuilder(req.Count, "", map[string]interface{}{
		"deck_id$eq":   req.DeckID,
		"has_drawn$eq": false,
	})

	tx := postgresql.DbSqlx.MustBegin()
	updatedCardDeck, err := postgresql.UpdateCardDeck(ctx, tx, queryBuilderUpdate)
	if len(updatedCardDeck) <= 0 || err != nil {
		tx.Rollback()
		return listResponseCardDeck, errors.New("deck is not found")
	}
	tx.Commit()

	queryBuilderSelect := util.NewQueryBuilder(0, "", map[string]interface{}{
		"deck_id$eq":   req.DeckID,
		"has_drawn$eq": true,
	})

	listCardDeck, err := postgresql.ListCardDeck(ctx, queryBuilderSelect)
	if err != nil {
		return listResponseCardDeck, err
	}

	for _, l := range listCardDeck {
		var d entity.Card
		d.Code = l.CardCode
		d.Suit = l.Suit
		d.Value = l.Value
		listResponseCardDeck.Cards = append(listResponseCardDeck.Cards, d)
	}
	return listResponseCardDeck, nil
}

func (d *deckRepo) CreateDeck(ctx context.Context, req entity.CreateDeckRequest) ([]entity.Deck, error) {
	var data []postgresql.Deck
	var listResponseDeck []entity.Deck

	cardOrderBy := ""
	cardFilter := map[string]interface{}{}

	if req.Cards != "" {
		cardFilter["code$in"] = strings.Split(strings.Trim(req.Cards, ""), ",")
	}

	if req.Shuffled {
		cardOrderBy = "random"
	}

	queryBuilderCards := util.NewQueryBuilder(0, cardOrderBy, cardFilter)

	cards, errListCard := postgresql.ListCard(ctx, queryBuilderCards)
	if errListCard != nil {
		return listResponseDeck, errListCard
	}

	u1 := uuid.Must(uuid.NewV1(), nil)
	data = append(data, postgresql.Deck{
		IsShuffle: req.Shuffled,
		ID:        u1.String(),
	})

	tx := postgresql.DbSqlx.MustBegin()
	decks, err := postgresql.CreateDeck(ctx, tx, data)
	if err != nil {
		tx.Rollback()
		return listResponseDeck, err
	}

	var listCreateCardDeck []postgresql.CardDeck
	for _, d := range decks {
		var cd postgresql.CardDeck
		cd.DeckID = d.ID
		for _, c := range cards {
			cd.CardCode = c.Code
			cd.HasDrawn = false
			cd.Suit = c.Suit
			cd.Value = c.Value
			listCreateCardDeck = append(listCreateCardDeck, cd)
		}
	}

	_, err = postgresql.CreateCardDeck(ctx, tx, listCreateCardDeck)
	if err != nil {
		tx.Rollback()
		return listResponseDeck, err
	}

	for _, m := range decks {
		var d entity.Deck
		d.DeckID = m.ID
		d.Shuffled = m.IsShuffle
		d.Remaining = len(cards)
		listResponseDeck = append(listResponseDeck, d)
	}
	tx.Commit()
	return listResponseDeck, nil
}

func NewDeckRepo() DeckContract {
	var d deckRepo
	return &d
}
