package postgresql

import "time"

type Deck struct {
	ID        string    `db:"id"`
	IsShuffle bool      `db:"is_shuffle"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

type Card struct {
	ID    int    `db:"id"`
	Code  string `db:"code"`
	Value string `db:"value"`
	Suit  string `db:"suit"`
}

type CardDeck struct {
	ID       int    `db:"id"`
	DeckID   string `db:"deck_id"`
	CardCode string `db:"card_code"`
	Value    string `db:"value"`
	Suit     string `db:"suit"`
	HasDrawn bool   `db:"has_drawn"`
}
