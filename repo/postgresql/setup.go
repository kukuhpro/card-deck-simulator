package postgresql

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jmoiron/sqlx"
)

var DbSqlx *sqlx.DB

func Setup() {
	DB_DSN := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", os.Getenv("postgresql_user"), os.Getenv("postgresql_password"), os.Getenv("postgresql_host"), os.Getenv("postgresql_port"), os.Getenv("postgresql_database"))
	db, errOpenConnection := sql.Open("postgres", DB_DSN)
	DbSqlx = sqlx.NewDb(db, "postgres")
	if errOpenConnection != nil {
		panic(errOpenConnection)
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		log.Fatalln(err)
	}
	m, errMigration := migrate.NewWithDatabaseInstance("file://repo/postgresql/migrations", "postgres", driver)
	if errMigration != nil {
		log.Println("Database is failed to migrate")
		log.Fatalln(errMigration)
	}
	log.Println("START MIGRATION...")
	m.Up()
	log.Println("FINISHED MIGRATION...")
}
