package postgresql

import (
	"bytes"
	"card-deck-simulation/util"
	"context"

	"github.com/jmoiron/sqlx"
)

func GetDeck(ctx context.Context, queryBuilder util.QueryBuilderContract) (Deck, error) {
	var res Deck
	q := `
		SELECT 
			id, 
			is_shuffle
		FROM
			decks
	`

	queryWhere, args := queryBuilder.Where()

	q += queryWhere

	if err := DbSqlx.GetContext(ctx, &res, DbSqlx.Rebind(q), args...); err != nil {
		return res, err
	}
	return res, nil
}

func CreateCardDeck(ctx context.Context, tx *sqlx.Tx, data []CardDeck) ([]CardDeck, error) {
	var res []CardDeck
	var args []interface{}
	values := new(bytes.Buffer)
	for k, d := range data {
		if k == 0 {
			values.WriteString("(?, ?, ?, ?, ?)")
		} else {
			values.WriteString(", (?, ?, ?, ?, ?)")
		}
		args = append(args, d.DeckID, d.CardCode, d.Value, d.Suit, d.HasDrawn)
	}
	valuesTxt := values.String()
	q := `
		INSERT INTO
			card_deck
				(
					deck_id,
					card_code,
					value,
					suit,
					has_drawn
				)
		VALUES
	` + valuesTxt

	q += `
		RETURNING
			id, 
			deck_id,
			card_code,
			value,
			suit,
			has_drawn
	`

	if err := tx.SelectContext(ctx, &res, DbSqlx.Rebind(q), args...); err != nil {
		return res, err
	}

	return res, nil
}

func CreateDeck(ctx context.Context, tx *sqlx.Tx, data []Deck) ([]Deck, error) {
	values := new(bytes.Buffer)
	var args []interface{}
	for k, d := range data {
		if k == 0 {
			values.WriteString("(?, ?)")
		} else {
			values.WriteString(", (?, ?)")
		}
		args = append(args, d.ID, d.IsShuffle)
	}
	valuesTxt := values.String()
	q := `
		INSERT INTO 
			decks
			(
                id,
                is_shuffle
			)
		VALUES
	` + valuesTxt

	q += `
        RETURNING
            id, 
            is_shuffle
	`

	var res []Deck
	if err := tx.SelectContext(ctx, &res, DbSqlx.Rebind(q), args...); err != nil {
		return res, err
	}
	return res, nil
}

func ListCardDeck(ctx context.Context, qb util.QueryBuilderContract) ([]CardDeck, error) {
	var res []CardDeck
	q := `
		SELECT
			id, 
			value, 
			suit, 
			card_code
		FROM card_deck
	`

	queryWhere, args := qb.Where()

	q += queryWhere + " ORDER BY id ASC "

	if err := DbSqlx.SelectContext(ctx, &res, DbSqlx.Rebind(q), args...); err != nil {
		return res, err
	}

	return res, nil
}

func UpdateCardDeck(ctx context.Context, tx *sqlx.Tx, qb util.QueryBuilderContract) ([]CardDeck, error) {
	var res []CardDeck
	q := `
		UPDATE card_deck
			SET 
				has_drawn=true,
				updated_at=NOW()
		WHERE id IN 
			(
				SELECT
					id
				FROM 
					card_deck
	`

	queryWhere, args := qb.Where()
	q += queryWhere + " ORDER BY id ASC " + qb.Limit()

	q += `
			)
		RETURNING
			id,
			value, 
			suit,
			card_code
	`
	if err := tx.SelectContext(ctx, &res, DbSqlx.Rebind(q), args...); err != nil {
		return res, err
	}
	return res, nil
}
