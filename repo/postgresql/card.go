// postrgesql connection and query handling
package postgresql

import (
	"card-deck-simulation/util"
	"context"
)

func ListCard(ctx context.Context, qb util.QueryBuilderContract) ([]Card, error) {
	var res []Card
	q := `
		SELECT 
			id, 
			code,
			value,
			suit
		FROM cards
	`

	whereQuery, args := qb.Where()

	q = q + whereQuery + qb.Order()
	if err := DbSqlx.SelectContext(ctx, &res, DbSqlx.Rebind(q), args...); err != nil {
		return res, err
	}

	return res, nil
}
