BEGIN;
CREATE TABLE card_deck (
    id SERIAL PRIMARY KEY NOT NULL,
    deck_id VARCHAR(100) NOT NULL,
    card_code VARCHAR(4) NOT NULL,
    value VARCHAR NOT NULL DEFAULT '', 
    suit VARCHAR NOT NULL DEFAULT '', 
    has_drawn BOOLEAN NOT NULL DEFAULT 'false',
    updated_at timestamp NOT NULL DEFAULT NOW(),
    created_at timestamp NOT NULL DEFAULT NOW()
);

CREATE INDEX index_6 ON card_deck(deck_id);
CREATE INDEX index_7 ON card_deck(card_code);
COMMIT;
