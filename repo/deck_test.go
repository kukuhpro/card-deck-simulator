package repo_test

import (
	"card-deck-simulation/entity"
	"card-deck-simulation/repo"
	"card-deck-simulation/repo/postgresql"
	"context"
	"database/sql"
	"errors"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"syreclabs.com/go/faker"
)

type DeckSuiteTest struct {
	suite.Suite
	db       *sql.DB
	mockSql  sqlmock.Sqlmock
	deckRepo repo.DeckContract
}

func (d *DeckSuiteTest) SetupTest() {
	db, mock, err := sqlmock.New()
	if err != nil {
		panic("Failed generate sql mock")
	}
	postgresql.DbSqlx = sqlx.NewDb(db, "postgres")
	d.mockSql = mock
	d.db = db
	d.deckRepo = repo.NewDeckRepo()
}

func TestDeckSuite(t *testing.T) {
	suite.Run(t, new(DeckSuiteTest))
}

func (d *DeckSuiteTest) TestGetShouldSuccessReturnDeck() {
	var req entity.OpenDeckRequest
	req.DeckID = faker.RandomString(8)
	mockSql := d.mockSql
	valueIsShuffled := false
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var card1 postgresql.CardDeck
	card1.ID = faker.Number().NumberInt(3)
	card1.DeckID = req.DeckID
	card1.CardCode = faker.RandomString(4)
	card1.HasDrawn = false
	card1.Value = faker.RandomString(8)
	card1.Suit = faker.RandomString(2)

	mockSql.ExpectQuery("SELECT id, is_shuffle FROM decks").WithArgs(req.DeckID).WillReturnRows(sqlmock.NewRows([]string{"id", "is_shuffle"}).AddRow(req.DeckID, valueIsShuffled))
	mockSql.ExpectQuery("SELECT id, value, suit, card_code FROM card_deck").WithArgs(req.DeckID, false).WillReturnRows(sqlmock.NewRows([]string{"id", "value", "suit", "card_code"}).AddRow(card1.ID, card1.Value, card1.Suit, card1.CardCode))

	res, err := d.deckRepo.Get(ctx, req)
	require.Equal(d.T(), nil, err)
	require.Equal(d.T(), req.DeckID, res.DeckID)
	require.Equal(d.T(), 1, len(res.Cards))
	card := res.Cards[0]
	require.Equal(d.T(), card1.CardCode, card.Code)
}

func (d *DeckSuiteTest) TestGetShouldError() {
	var req entity.OpenDeckRequest
	req.DeckID = faker.RandomString(8)
	valueIsShuffled := false
	mockSql := d.mockSql
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	mockSql.ExpectQuery("SELECT id, is_shuffle FROM decks").WithArgs(req.DeckID).WillReturnError(errors.New("Connection Timeout"))

	_, err := d.deckRepo.Get(ctx, req)
	require.NotEqual(d.T(), nil, err)
	require.IsType(d.T(), errors.New(""), err)

	mockSql.ExpectQuery("SELECT id, is_shuffle FROM decks").WithArgs(req.DeckID).WillReturnRows(sqlmock.NewRows([]string{"id", "is_shuffle"}).AddRow(req.DeckID, valueIsShuffled))
	mockSql.ExpectQuery("SELECT id, value, suit, card_code FROM card_deck").WithArgs(req.DeckID, false).WillReturnError(errors.New("Connection Timeout"))
	res, err := d.deckRepo.Get(ctx, req)

	require.NotEqual(d.T(), nil, err)
	require.IsType(d.T(), errors.New(""), err)
	require.Equal(d.T(), req.DeckID, res.DeckID)
}

func (d *DeckSuiteTest) TestCreateDeckShouldSuccess() {
	var req entity.CreateDeckRequest
	req.Cards = "a,b,c"
	req.Shuffled = true
	uuid := uuid.Must(uuid.NewV1(), nil)

	mockSql := d.mockSql
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var card1 postgresql.CardDeck
	card1.ID = faker.RandomInt(0, 100)
	card1.CardCode = faker.RandomString(2)
	card1.HasDrawn = false
	card1.Suit = faker.RandomString(4)
	card1.Value = faker.Number().Number(2)

	mockSql.ExpectQuery("SELECT id, code, value, suit FROM cards").WithArgs("a", "b", "c").WillReturnRows(sqlmock.NewRows([]string{"id", "code", "value", "suit"}).AddRow(card1.ID, card1.CardCode, card1.Suit, card1.Value))

	mockSql.ExpectBegin()

	uuidText := uuid.String()
	mockSql.ExpectQuery(`INSERT INTO decks`).WillReturnRows(sqlmock.NewRows([]string{"id", "is_shuffle"}).AddRow(uuidText, req.Shuffled))
	mockSql.ExpectQuery("INSERT INTO card_deck").WillReturnRows(sqlmock.NewRows([]string{"id", "deck_id", "card_code", "value", "has_drawn"}).AddRow(card1.ID, uuidText, card1.CardCode, card1.Value, false))

	mockSql.ExpectCommit()

	res, err := d.deckRepo.CreateDeck(ctx, req)
	require.Equal(d.T(), nil, err)
	require.Equal(d.T(), true, len(res) > 0)
	require.Equal(d.T(), uuidText, res[0].DeckID)
}

func (d *DeckSuiteTest) TestCreateDeckShouldError() {
	var req entity.CreateDeckRequest
	req.Cards = "a,b,c"
	req.Shuffled = true
	uuid := uuid.Must(uuid.NewV1(), nil)

	mockSql := d.mockSql
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var card1 postgresql.CardDeck
	card1.ID = faker.RandomInt(0, 100)
	card1.CardCode = faker.RandomString(2)
	card1.HasDrawn = false
	card1.Suit = faker.RandomString(4)
	card1.Value = faker.Number().Number(2)

	mockSql.ExpectQuery("SELECT id, code, value, suit FROM cards").WithArgs("a", "b", "c").WillReturnRows(sqlmock.NewRows([]string{"id", "code", "value", "suit"}).AddRow(card1.ID, card1.CardCode, card1.Suit, card1.Value))

	mockSql.ExpectBegin()

	uuidText := uuid.String()
	mockSql.ExpectQuery(`INSERT INTO decks`).WillReturnError(errors.New("Connection timeout"))
	mockSql.ExpectRollback()
	_, err := d.deckRepo.CreateDeck(ctx, req)
	require.IsType(d.T(), errors.New(""), err)

	mockSql.ExpectBegin()
	mockSql.ExpectQuery(`INSERT INTO decks`).WillReturnRows(sqlmock.NewRows([]string{"id", "is_shuffle"}).AddRow(uuidText, req.Shuffled))
	mockSql.ExpectQuery("INSERT INTO card_deck").WillReturnError(errors.New("Connection timeout"))
	mockSql.ExpectRollback()
	_, err = d.deckRepo.CreateDeck(ctx, req)

	require.IsType(d.T(), errors.New(""), err)
}

func (d *DeckSuiteTest) TestCardSubtractShouldSuccess() {
	var req entity.DrawCardRequest
	req.DeckID = faker.RandomString(8)
	req.Count = 1

	mockSql := d.mockSql
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var cardDeck1 postgresql.CardDeck
	cardDeck1.ID = faker.RandomInt(1, 10)
	cardDeck1.DeckID = req.DeckID
	cardDeck1.Value = faker.RandomString(6)
	cardDeck1.Suit = faker.RandomString(2)
	cardDeck1.CardCode = faker.RandomString(3)

	mockSql.ExpectBegin()
	mockSql.ExpectQuery("UPDATE card_deck").WithArgs(req.DeckID, false).WillReturnRows(sqlmock.NewRows([]string{"id", "value", "suit", "card_code"}).AddRow(cardDeck1.ID, cardDeck1.Value, cardDeck1.Suit, cardDeck1.CardCode))
	mockSql.ExpectCommit()

	mockSql.ExpectQuery("SELECT id, value, suit, card_code FROM card_deck").WithArgs(req.DeckID, true).WillReturnRows(sqlmock.NewRows([]string{"id", "value", "suit", "card_code"}).AddRow(cardDeck1.ID, cardDeck1.Value, cardDeck1.Suit, cardDeck1.CardCode))

	res, err := d.deckRepo.CardSubtract(ctx, req)
	require.Equal(d.T(), nil, err)
	require.Equal(d.T(), req.DeckID, res.DeckID)
}
